/* copyright etc
*
* A simple PoC for the SamKnows technical test.
*
* Use libcurl easy interface to obtain stats for www.google.com
* responding to HTTP GET.
*
*
*/

#define _GNU_SOURCE

#include <curl/curl.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct statistics {
    int sample_id;
    char *ip;
    long response_code;
    double name_lookup_time;
    double connect_time;
    double start_transfer_time;
    double total_time;
} statistics_t;

typedef struct node {
    statistics_t stats;
    struct node *next;
} node_t;

typedef struct result {
    double result;
    double *base;
} result_t;

enum statistic {
    NAME_LOOKUP,
    CONNECT,
    START_TRANSFER,
    TOTAL
};

static void set_curl_options(CURL * curl);
static void get_statistics(CURL * curl, statistics_t * ps);
static void output_statistics(FILE * fp, node_t * pn, result_t * name,
			      result_t * connect, result_t * start,
			      result_t * total);
static void free_list(node_t * pn);
static int compare(const void *first, const void *second);	/* for qsort */
static void initialise_result_set(const unsigned long, result_t * ps);
static void release_result_set(result_t * ps);
static void populate_result_set(node_t * head, result_t * ps,
				enum statistic st);
static void calculate_median(result_t * ps);

unsigned long ntimes;
unsigned int mode;
node_t *head;

int main(int argc, char *argv[])
{
    CURL *curl;
    CURLcode rc;

    result_t name_lookup;
    result_t connect;
    result_t start_transfer;
    result_t total;

    /* how many samples? */
    ntimes = 4;

    initialise_result_set(ntimes, &name_lookup);
    initialise_result_set(ntimes, &connect);
    initialise_result_set(ntimes, &start_transfer);
    initialise_result_set(ntimes, &total);

    rc = curl_global_init(CURL_GLOBAL_ALL);
    if (rc != CURLE_OK) {

	fprintf(stdout, "curl global initialise failed\n");
	exit(1);
    }

    curl = curl_easy_init();
    if (!curl) {
	fprintf(stderr, "curl initialise failed\n");
	exit(1);
    }

    /* set options */
    set_curl_options(curl);

    /* get the samples n times */
    {
	int i;
	node_t *node;
	node_t *prev;
	for (i = 0; i < ntimes; ++i) {
	    curl_easy_perform(curl);
	    node = (node_t *) malloc(sizeof(*node));
	    if (!node) {
		fprintf(stderr, "memory allocation failed\n");
		exit(2);
	    }
	    if (!head) {
		head = node;
		head->next = NULL;
	    } else {
		prev = head;
		head = node;
		head->next = prev;
	    }
	    head->stats.sample_id = i;
	    get_statistics(curl, &head->stats);
	}

    }

    /* sample data is in our list
     *  we need to put it into something we can sort
     *  and then process to find the median values
     */

    /*
     *  scan each list and place into the result set
     *  call qsort on each set
     *  calculate median
     */

    populate_result_set(head, &name_lookup, NAME_LOOKUP);
    populate_result_set(head, &connect, CONNECT);
    populate_result_set(head, &start_transfer, START_TRANSFER);
    populate_result_set(head, &total, TOTAL);

    calculate_median(&name_lookup);
    calculate_median(&connect);
    calculate_median(&start_transfer);
    calculate_median(&total);

    output_statistics(stdout, head, &name_lookup, &connect,
		      &start_transfer, &total);
    free_list(head);

    release_result_set(&name_lookup);
    release_result_set(&connect);
    release_result_set(&start_transfer);
    release_result_set(&total);

    curl_easy_cleanup(curl);

    return EXIT_SUCCESS;
}


void set_curl_options(CURL * curl)
{
    CURLcode rc;

    rc = curl_easy_setopt(curl, CURLOPT_URL, "http://www.google.com");
    if (rc != CURLE_OK) {
	fprintf(stderr, "bad curl option - url\n");
	exit(2);
    }

    rc = curl_easy_setopt(curl, CURLOPT_NOBODY, 1L);	/* HEAD ONLY */
    if (rc != CURLE_OK) {
	fprintf(stderr, "bad curl option - no body\n");
	exit(2);
    }

}


void output_statistics(FILE * fp, node_t * pn, result_t * name,
		       result_t * connect, result_t * start,
		       result_t * total)
{
    if (!pn) {
	fprintf(stderr, "invalid node\n");
	return;
    }

    if (mode) {

	/* scan the list */
	while (pn->next) {
	    fprintf(fp, "SKTEST:%s;%ld;%g;%g;%g;%g\n",
		    pn->stats.ip,
		    pn->stats.response_code,
		    pn->stats.name_lookup_time,
		    pn->stats.connect_time,
		    pn->stats.start_transfer_time, pn->stats.total_time);
	    pn = pn->next;
	}
	/* the last item in our list */
	if (pn) {
	    fprintf(fp, "SKTEST:%s;%ld;%g;%g;%g;%g\n",
		    pn->stats.ip,
		    pn->stats.response_code,
		    pn->stats.name_lookup_time,
		    pn->stats.connect_time,
		    pn->stats.start_transfer_time, pn->stats.total_time);
	}
    }

    fprintf(fp, "SKTEST:%s;%ld;%g;%g;%g;%g\n",
	    pn->stats.ip,
	    pn->stats.response_code,
	    name->result, connect->result, start->result, total->result);


}

void get_statistics(CURL * curl, statistics_t * ps)
{
    CURLcode rc;


    rc = curl_easy_getinfo(curl, CURLINFO_PRIMARY_IP, &ps->ip);
    if (rc != CURLE_OK) {
	fprintf(stderr, "unable to obtain primary ip address\n");
    }

    rc = curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE,
			   &ps->response_code);
    if (rc != CURLE_OK) {
	fprintf(stderr, "unable to obtain http response code\n");
    }

    rc = curl_easy_getinfo(curl, CURLINFO_NAMELOOKUP_TIME,
			   &ps->name_lookup_time);
    if (rc != CURLE_OK) {
	fprintf(stderr, "unable to obtain name lookup time\n");
    }

    rc = curl_easy_getinfo(curl, CURLINFO_CONNECT_TIME, &ps->connect_time);
    if (rc != CURLE_OK) {
	fprintf(stderr, "unable to obtain connect time\n");
    }

    rc = curl_easy_getinfo(curl, CURLINFO_STARTTRANSFER_TIME,
			   &ps->start_transfer_time);
    if (rc != CURLE_OK) {
	fprintf(stderr, "unable to obtain start transfer time\n");
    }

    rc = curl_easy_getinfo(curl, CURLINFO_TOTAL_TIME, &ps->total_time);
    if (rc != CURLE_OK) {
	fprintf(stderr, "unable to obtain total time\n");
    }
}

void free_list(node_t * pn)
{
    node_t *n;

    while (pn->next) {
	n = pn->next;;
	free(pn);
	pn = n;
    }
    if (pn) {
	free(pn);
    }
}


int compare(const void *first, const void *second)
{
    int res;

    const double *d1 = (double *) first;
    const double *d2 = (double *) second;

    if (*d1 > *d2) {
	res = 1;
    } else {
	res = -1;
    }
    return res;
}

void initialise_result_set(const unsigned long n, result_t * ps)
{
    ps->base = (double *) malloc(n * sizeof(double *));
    ps->result = 0.0f;
}

void populate_result_set(node_t * head, result_t * ps, enum statistic st)
{
    node_t *pn;
    double *pd;

    if (!ps) {
	fprintf(stderr, "invalid result set\n");
	exit(2);
    }

    pn = head;
    pd = ps->base;
    while (pn->next) {
	switch (st) {
	case NAME_LOOKUP:
	    *pd = pn->stats.name_lookup_time;
	    break;
	case CONNECT:
	    *pd = pn->stats.connect_time;
	    break;
	case START_TRANSFER:
	    *pd = pn->stats.start_transfer_time;
	    break;
	case TOTAL:
	    *pd = pn->stats.total_time;
	    break;
	default:
	    fprintf(stderr, "bad value in switch %d\n", st);
	}
	++pd;
	pn = pn->next;
    }
    switch (st) {
    case NAME_LOOKUP:
	*pd = pn->stats.name_lookup_time;
	break;
    case CONNECT:
	*pd = pn->stats.connect_time;
	break;
    case START_TRANSFER:
	*pd = pn->stats.start_transfer_time;
	break;
    case TOTAL:
	*pd = pn->stats.total_time;
	break;
    default:
	fprintf(stderr, "bad value in switch %d\n", st);
    }

    qsort(ps->base, (size_t) ntimes, sizeof(*pd), compare);
}

void calculate_median(result_t * ps)
{
    unsigned long offset;
    int odd;

    odd = ntimes % 2;
    offset = (ntimes / 2);
    if (odd) {
	ps->result = *((ps->base) + offset);
    } else {
	ps->result = (*(ps->base + offset) + *(ps->base + offset - 1)) / 2;
    }
}

void release_result_set(result_t * ps)
{
    if (ps)
	free(ps->base);
}
