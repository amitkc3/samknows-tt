CC=gcc
CFLAGS=-ggdb3 -std=c1x -Wall -Werror

all: simple libsktest samk

samk: samk.o
	$(CC) $(CFLAGS) -o $@ $< -L. -lsktest

samk.o: samk.c sktest.h
	$(CC) -c $(CFLAGS) -o $@ $<

libsktest: sktest.o
	$(CC) -shared -o libsktest.so.1.0.0 sktest.o -lcurl
	ln -sf libsktest.so.1.0.0 libsktest.so
	ln -sf libsktest.so.1.0.0 libsktest.so.1

sktest.o: sktest.c sktest.h
	$(CC) -c $(CFLAGS) -fPIC -o $@ $<

memcheck: samk
	valgrind --leak-check=full --show-leak-kinds=all ./$< -n 2 

simple: main.o curl.h 
	$(CC) $(CFLAGS) -o $@ main.o -lcurl

main.o: main.c
	$(CC) -c $(CFLAGS) -o $@ $<

curl.h:

.PHONY: clean

clean:
	rm -f simple libsktest* *.o samk
