/* copyright etc
*
* A an application to perform the SamKnows technical test.
*
* Use libcurl easy interface to obtain stats for www.google.com
* responding to HTTP GET.
*
*/

#define _GNU_SOURCE

#include "sktest.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static void usage(void);
static void process_args(int count, char **args, char **ph_options);

static unsigned long ntimes;

int main(int argc, char *argv[])
{
    char **ph_options;

    ph_options = (char **) malloc((size_t) argc * sizeof(char *));
    process_args(argc, argv, ph_options);

    if (!ntimes) {
	fprintf(stderr,
		"ntimes must be a positive integer specified with the -n switch\n");
	fprintf(stderr, "running with -n 1\n");
	ntimes = 1;
    }

    sk_get_samples(ntimes, ph_options);

    free(ph_options);

    return EXIT_SUCCESS;
}

void process_args(int count, char **args, char **pheaders)
{
    int i;
    int count_h_options;

    if (count == 1 ||count == 2) {
	usage();
    }

    i = count;
    count_h_options = 0;
    while (--i) {
	++args;
	if (!strcmp(*args, "-n")) {
	    ntimes = strtoul(*(args + 1), NULL, 0);
	    if (!ntimes) {
		fprintf(stderr,
			"Over-riding invalid argument: must be a positive integer\n");
		ntimes = 1;
	    }
	    continue;
	}
	if (!strcmp(*args, "-H")) {
	    pheaders[count_h_options] = *(args + 1);
	    count_h_options++;
	    continue;
	}
    }
    pheaders[count_h_options] = NULL;
}

void usage(void)
{
    fprintf(stderr,
	    "usage: ./samk [-n <number of samples>] [-H \"header:value\"] [-H \"header:value\"]...\n");
    exit(1);
}
